name := "weather-balloon-simulator"

version := "0.1-SNAPSHOT"

scalaVersion := "2.11.8"

val sparkVersion = "2.3.0"

val specs2Version = "4.0.1"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-hive" % sparkVersion % "provided",
  "org.slf4j" % "slf4j-api" % "1.7.10",
  "com.typesafe" % "config" % "1.3.1",
  "org.apache.spark" %% "spark-hive" % sparkVersion % "provided",
  "org.specs2" %% "specs2-core" % "4.0.1" % Test,
  "org.specs2" %% "specs2-mock" % "4.0.0" % Test)
package com.au.weathersimulator.testutils

import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory
import org.specs2.mutable.Specification

class TestUtils extends Specification {

  val logger = LoggerFactory.getLogger(classOf[TestUtils])

  implicit val sparkSession: SparkSession = {
    logger.info(s"Creating spark session for tests")
    SparkSession
      .builder()
      .appName(getClass.getName)
      .master("local[*]")
      .getOrCreate()
  }

}

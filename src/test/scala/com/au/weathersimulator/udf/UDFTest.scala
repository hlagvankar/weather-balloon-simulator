package com.au.weathersimulator.udf

import com.au.weathersimulator.UDF.UserDefinedFunctions
import com.au.weathersimulator.testutils.TestUtils
import org.apache.spark.sql.functions._

class UDFTest extends TestUtils {

  sequential

  "udf udf_temp_normalize" should {
    "return normalize temperature in celsius" in {
      import sparkSession.implicits._
      val df = Seq(("40", "AU")).toDF("temperature", "observatory")
      df.show(false)
      val result = df.select(UserDefinedFunctions.udf_temp_normalize(col("TEMPERATURE"), col("OBSERVATORY")).as("TEMPERATURE"))
      result.take(1)(0).getDouble(0) must_== 40.0
    }

    "return normalize temperature in kelvin" in {
      import sparkSession.implicits._
      val df = Seq(("40", "IN")).toDF("temperature", "observatory")
      df.show(false)
      val result = df.select(UserDefinedFunctions.udf_temp_normalize(col("TEMPERATURE"), col("OBSERVATORY")).as("TEMPERATURE"))
      result.take(1)(0).getDouble(0) must_== 313.15
    }

    "return normalize temperature in fahrenheit" in {
      import sparkSession.implicits._
      val df = Seq(("40", "US")).toDF("temperature", "observatory")
      df.show(false)
      val result = df.select(UserDefinedFunctions.udf_temp_normalize(col("TEMPERATURE"), col("OBSERVATORY")).as("TEMPERATURE"))
      result.take(1)(0).getDouble(0) must_== 104.0
    }
  }

  "udf udf_get_temperature_unit" should {
    "return temperature unit celsius" in {
      import sparkSession.implicits._
      val df = Seq("AU").toDF("observatory")
      df.show(false)
      val result = df.select(UserDefinedFunctions.udf_get_temperature_unit(col("OBSERVATORY")).as("UNIT"))
      result.take(1)(0).getString(0) must_== "celsius"
    }

    "return temperature unit kelvin" in {
      import sparkSession.implicits._
      val df = Seq("IN").toDF("observatory")
      df.show(false)
      val result = df.select(UserDefinedFunctions.udf_get_temperature_unit(col("OBSERVATORY")).as("UNIT"))
      result.take(1)(0).getString(0) must_== "kv"
    }
  }

  "udf udf_get_location_unit" should {
    "return location unit miles" in {
      import sparkSession.implicits._
      val df = Seq("US").toDF("observatory")
      df.show(false)
      val result = df.select(UserDefinedFunctions.udf_get_location_unit(col("OBSERVATORY")).as("UNIT"))
      result.take(1)(0).getString(0) must_== "miles"
    }

    "return location unit kilometer" in {
      import sparkSession.implicits._
      val df = Seq("IN").toDF("observatory")
      df.show(false)
      val result = df.select(UserDefinedFunctions.udf_get_location_unit(col("OBSERVATORY")).as("UNIT"))
      result.take(1)(0).getString(0) must_== "km"
    }
  }

}

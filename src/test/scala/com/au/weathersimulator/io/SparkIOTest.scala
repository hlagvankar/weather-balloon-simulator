package com.au.weathersimulator.io

import com.au.weathersimulator.testutils.TestUtils
import com.au.weathersimulator.utils.AppConfig
import org.specs2.mock.Mockito

class SparkIOTest extends TestUtils with Mockito{

  sequential

  "The function readCSV" should {
    "return an exception when the path doesn't exist" in {
      val inputPath = "adss/asdsa"
      val configString =
        """
          |run{
          |}
        """.stripMargin

      val config = new AppConfig(configString)
      val sparkIO = new SparkIO
      sparkIO.readCSV(inputPath) must throwA[SparkReadException]
    }

    "return one dataset when reading" in {
      val filePath = "src/test/resources/readCSVInput1"
      val sparkIO = new SparkIO
      val dataset = sparkIO.readCSV(filePath)
      dataset.count() must_== 3
    }
  }

  "The function writeCSV" should {
    "throw an exception when unknown protocol" in {
      import sparkSession.implicits._
      val sparkIO = new SparkIO
      val dataset = Seq("test").toDF("col1")
      val output = "XXX://src/test/resources/writeCSVOutput"
      sparkIO.writeCSV(output, dataset) must throwAn[SparkWriteException]
    }

    "write a CSV file to specified path" in {
      import sparkSession.implicits._
      val sparkIO = new SparkIO
      val dataset = Seq("test").toDF("col1")
      val filePath = "src/test/resources/writeCSVOutput"
      val outputPath = sparkIO.writeCSV(filePath, dataset)
      sparkIO.readCSV(filePath).count() must_== 1
      outputPath must_== filePath
    }
  }
}

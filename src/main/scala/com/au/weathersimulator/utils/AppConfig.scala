package com.au.weathersimulator.utils

import com.typesafe.config.{Config, ConfigFactory}

/*
 * Configuration class to read application.conf file or configuration passed through spark-submit
 * Not needed currently.
 */
object AppConfig {
  //def init(outputPath: String, numberOfRecords: Int) = new AppConfig(outputPath, numberOfRecords)
  def init(configString: String) = new AppConfig(configString)
}

class AppConfig(configString: String) {
  private val config: Config = ConfigFactory.load()

  // To be implemented. In future you can read configuration from CONF file here
}
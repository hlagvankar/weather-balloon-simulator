package com.au.weathersimulator.utils

import java.sql.Timestamp

import com.au.weathersimulator.simulator._
import com.au.weathersimulator.simulator.Observatories._
import org.apache.spark.SparkContext
import org.apache.spark.sql.{Column, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._

import scala.util.{Random, Try}

/*
 * Common functions can be implemented here.
 */
object common {
  val badRecordRate = 0.1

  def writeWeatherData(numberOfRecords: Int, sc: SparkContext, spark: SparkSession): Dataset[Row] = {
    import spark.implicits._
    sc.parallelize(Seq.fill(numberOfRecords)(
      if(badRecords) null else Timestamp.valueOf(timestampGen.generate),
      if(badRecords) null else generateLocation.generate._1 + "," + generateLocation.generate._2,
      temperatureGen.generate,
      if(badRecords) null else generateObeservatory.generate
    )).toDF("timestamp", "location", "temperature", "observatory")
  }

  def badRecords: Boolean = {
    if (Random.nextFloat() < badRecordRate)
      true
    else
      false
  }

  def isValidDate(column: Timestamp): Boolean = {
    if(column != null)
      true
    else
      false
  }

  def isValidLocation(column: String): Boolean = {
    if(column != null)
      true
    else
      false
  }

  def isValidObs(column: String): Boolean = {
    val observatories = List(AUSTRALIA, UNITED_STATES, FRANCE, ITALY, INDIA, SINGAPORE, CANADA)
    if(observatories.contains(column))
      true
    else
      false
  }
}

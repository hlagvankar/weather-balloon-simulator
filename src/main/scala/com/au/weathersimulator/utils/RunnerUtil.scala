package com.au.weathersimulator.utils

import org.slf4j.LoggerFactory

/*
 * This object calculates number of arguments passed to this application.
 */
object RunnerUtil {

  private val logger = LoggerFactory.getLogger(RunnerUtil.getClass)

  type OptionMap = Map[Symbol, String]

  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil => map
      case "--in-bucket" :: value :: tail =>
        nextOption(map ++ Map('inBucket -> value.toString), tail)
      case "--out-bucket" :: value :: tail =>
        nextOption(map ++ Map('outBucket -> value.toString), tail)
      case "--number-of-records" :: value :: tail =>
        nextOption(map ++ Map('numberOfRecords -> value.toString), tail)
      case "--conf" :: value :: tail =>
        nextOption(map ++ Map('confPath -> value.toString), tail)
      case "--app-name" :: value :: tail =>
        nextOption(map ++ Map('appName -> value.toString), tail)
      case "--local-mode" :: value :: tail =>
        nextOption(map ++ Map('mode -> value.toString), tail)
      case "--debug" :: value :: tail =>
        nextOption(map ++ Map('debug -> value.toString), tail)
      case option :: _ => logger.info("Unknown option " + option)
        sys.exit(1)
    }
  }
}

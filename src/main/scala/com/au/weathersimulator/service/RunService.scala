package com.au.weathersimulator.service

import com.au.weathersimulator.io.SparkIO
import com.au.weathersimulator.simulator.WeatherSimulator
import com.au.weathersimulator.utils.AppConfig
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.slf4j.LoggerFactory
import org.apache.spark.sql.functions._

/*
 * Processing starts here.
 * 1. Generate input set of records using SPARK Dataset API
 * 2. Read the generated input.
 * 3. Calculate various statistics.
 */
object RunService {

  private val logger = LoggerFactory.getLogger(RunService.getClass)

  def runSparkJobs(appConfig: AppConfig,
                   sparkIO: SparkIO,
                   appName: Option[String] = None,
                   inputPath: String,
                   outputPath: String,
                   numberOfRecords: Int,
                   sc: SparkContext)
                  (implicit sparkSession: SparkSession): Unit = {

    val weatherSimulator = WeatherSimulator

    logger.info(s"Started weather service...")
    weatherSimulator.execute(inputPath, numberOfRecords, sparkSession, sc)
    val weatherService = new WeatherService

    // Filter out bad records
    val badRecords = weatherService.readInput(appConfig, sparkIO, inputPath, true)
    badRecords.show(100 ,false)

    // Read input file now
    val input = weatherService.readInput(appConfig, sparkIO, inputPath).persist(StorageLevel.MEMORY_AND_DISK)
    input.show(100 ,false)

    // Calculate daily minimum temperature
    val minimumTemperature = weatherService.calculateMinTemp(input)
    minimumTemperature.show(100, false)

    // Calculate maximum temperature dailywise
    val maximumTemperature = weatherService.calculateMaxTemp(input)
    maximumTemperature.show(100, false)

    // Calculate total number of observations from each observatory
    val numberOfObservations = weatherService.calculateNumberOfObservations(input)
    numberOfObservations.show(100 ,false)

    // Calculate daily mean temperature
    val meanTemperatureDailyWise = weatherService.calculateDailyMeanTemperature(input)
    meanTemperatureDailyWise.show(100, false)

    // Calculate total distance travelled
    val totalDistance = weatherService.calculateDistanceTravelled(input)
    totalDistance.show(100, false)

    val normalizeOutput = weatherService.normalizeOutput(input)
    normalizeOutput.show(100, false)

    // Write datasets one by one
    sparkIO.writeCSV(outputPath + "/badRecords", badRecords)
    sparkIO.writeCSV(outputPath + "/minimumTemp", minimumTemperature)
    sparkIO.writeCSV(outputPath + "/maximumTemp", maximumTemperature)
    sparkIO.writeCSV(outputPath + "/meanTemp", meanTemperatureDailyWise)
    sparkIO.writeCSV(outputPath + "/numberOfObservations", numberOfObservations)
    sparkIO.writeCSV(outputPath + "/totalDistance", totalDistance)
    sparkIO.writeCSV(outputPath + "/normalizeOutput", normalizeOutput)
  }
}

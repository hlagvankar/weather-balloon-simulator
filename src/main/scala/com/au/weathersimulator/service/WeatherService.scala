package com.au.weathersimulator.service

import com.au.weathersimulator.io.SparkIO
import com.au.weathersimulator.utils.AppConfig
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.slf4j.LoggerFactory
import org.apache.spark.sql.functions._
import com.au.weathersimulator.UDF.UserDefinedFunctions._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types.DateType
import com.au.weathersimulator.utils.common._

/*
 * This class performs various operations on input dataset.
 */
class WeatherService {

  private val logger = LoggerFactory.getLogger(classOf[WeatherService])

  logger.info(s"Started weather service..")

  // Read CSV from input path
  def readInput(appConfig: AppConfig, sparkIO: SparkIO, inputPath: String, badRecords: Boolean = false)(implicit sparkSession: SparkSession): Dataset[Row] = {
    logger.info(s"Reading input file from $inputPath")
    val inputDataset: Dataset[Row] = sparkIO.readCSV(inputPath)
    if (badRecords)
      inputDataset
        .filter(c => !isValidDate(c.getTimestamp(0)) || !isValidLocation(c.getString(1)) || !isValidObs(c.getString(3)))
    else
      inputDataset
        .filter(c => isValidDate(c.getTimestamp(0)) && isValidLocation(c.getString(1)) && isValidObs(c.getString(3)))
  }

  // Calculate minimum temperature per observatory date wise.
  def calculateMinTemp(inputDataset: Dataset[Row]): Dataset[Row] = {
    logger.info(s"Calculate minimum temperature")
    inputDataset
      .groupBy(col("OBSERVATORY"), col("TIMESTAMP").cast(DateType).as("DATE"))
      .agg(min(udf_temp_normalize(col("TEMPERATURE"), col("OBSERVATORY"))).as("MINIMUM_TEMPERATURE"))
      .select(
        col("OBSERVATORY"),
        col("DATE"),
        bround(col("MINIMUM_TEMPERATURE"), 2).as("MINIMUM_TEMPERATURE"),
        udf_get_temperature_unit(col("OBSERVATORY")).as("UNIT")
      )
  }

  // Calculate maximum temperature per observatory date wise.
  def calculateMaxTemp(inputDataset: Dataset[Row]): Dataset[Row] = {
    logger.info(s"Calculate maximum temperature")
    inputDataset
      .groupBy(col("OBSERVATORY"), col("TIMESTAMP").cast(DateType).as("DATE"))
      .agg(max(udf_temp_normalize(col("TEMPERATURE"), col("OBSERVATORY"))).as("MAXIMUM_TEMPERATURE"))
      .select(
        col("OBSERVATORY"),
        col("DATE"),
        bround(col("MAXIMUM_TEMPERATURE"), 2).as("MAXIMUM_TEMPERATURE"),
        udf_get_temperature_unit(col("OBSERVATORY")).as("UNIT")
      )
  }

  // Calculate number of observations for each observatory.
  def calculateNumberOfObservations(inputDataset: Dataset[Row]): Dataset[Row] = {
    logger.info(s"Calculate number of observations per observatory")
    inputDataset
      .groupBy("OBSERVATORY")
      .agg(
        count("*").as("COUNT")
      )
      .select(col("*"))

  }

  // Calculate mean temperature per observatory date wise.
  def calculateDailyMeanTemperature(inputDataset: Dataset[Row]): Dataset[Row] = {
    logger.info(s"Calculate daily mean temperature")
    inputDataset
      .groupBy(col("OBSERVATORY"), col("TIMESTAMP").cast(DateType).as("DATE"))
      .agg(
        max(udf_temp_normalize(col("TEMPERATURE"), col("OBSERVATORY"))).as("MAXIMUM_TEMPERATURE"),
        min(udf_temp_normalize(col("TEMPERATURE"), col("OBSERVATORY"))).as("MINIMUM_TEMPERATURE")
      )
      .select(
        col("OBSERVATORY"),
        col("DATE"),
        bround((col("MAXIMUM_TEMPERATURE") + col("MINIMUM_TEMPERATURE")) / 2, 2).as("MEAN_TEMPERATURE"),
        udf_get_temperature_unit(col("OBSERVATORY")).as("UNIT")
      )
  }

  def calculateDistanceTravelled(inputDataset: Dataset[Row])(implicit spark: SparkSession): Dataset[Row] = {
    logger.info(s"Calculate distance travelled")
    import spark.implicits._
    inputDataset.map(r => r.getString(1).split(",").map(x => Location(x(0), x(1)))).printSchema()

    val result =
      inputDataset
        .withColumn("RANK", row_number() over Window
          .partitionBy(Seq(col("OBSERVATORY")): _*)
          .orderBy(Seq(desc("TIMESTAMP")): _*)
        ).map { records =>
        val x = records.getString(1).split(",")(0).toFloat
        val y = records.getString(1).split(",")(1).toFloat
        val observatory = records.getString(3)
        calculateDistance((x, y), observatory)
      }.toDF("DISTANCE", "OBSERVATORY", "UNIT")
        .groupBy("OBSERVATORY", "UNIT")
        .agg(sum("DISTANCE").as("TOTAL_DISTANCE"))

    result

  }

  def normalizeOutput(inputDataset: Dataset[Row]): Dataset[Row] = {
    logger.info(s"Normalizing output data")
   val result = inputDataset
      .select(
        col("TIMESTAMP"),
        udf_convert_distance(col("LOCATION"), col("OBSERVATORY")).as("LOCATION"),
        udf_temp_normalize(col("TEMPERATURE"), col("OBSERVATORY")).as("TEMPERATURE"),
        col("OBSERVATORY"),
        udf_get_temperature_unit(col("OBSERVATORY")).as("TEMP_UNIT"),
        udf_get_location_unit(col("OBSERVATORY")).as("LOCATION_UNIT")
      )

    result
  }

}

case class Location(latitude: Double, longitude: Double)

case class Observatory(code: String)

package com.au.weathersimulator.generator

trait Generator[T] {
  def generate: T
}

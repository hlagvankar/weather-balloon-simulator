package com.au.weathersimulator.io

import com.au.weathersimulator.utils.AppConfig
import org.apache.spark.sql.{Dataset, Row, SaveMode, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}
import org.slf4j.LoggerFactory
/*
 * This class performs SPARK related operations like
 * 1. Creating SparkContext/SparkSession.
 * 2. Reading CSV file.
 * 3. Writing CSV file.
 */

case class SparkReadException(message: String, cause: Throwable) extends Exception(message, cause)

case class SparkWriteException(message: String, cause: Throwable) extends Exception(message, cause)

class SparkIO() {

  private val logger = LoggerFactory.getLogger(classOf[SparkIO])

  def createSparkSession(appConfig: AppConfig, conf: SparkConf, appName: String): SparkSession = {
    val sparkSession = SparkSession
      .builder()
      .config(conf)
      .enableHiveSupport()
      .getOrCreate()

    sparkSession
  }

  def createSparkContext(appConfig: AppConfig, conf: SparkConf, appName: String): SparkContext = {
    new SparkContext(conf)
  }

  def readCSV(inputPath: String, multiLine: Boolean = false)(implicit sparkSession: SparkSession): Dataset[Row] = {
    logger.info(s"Reading files from $inputPath with multiline set to ${multiLine.toString}")
    try {
      val dataset = sparkSession
        .read
        .option("header", "true")
        .option("delimiter", "|")
        .option("inferSchema", "true")
        .option("multiLine", multiLine.toString)
        .csv(inputPath)

      logger.info(s"Number of partitions for $inputPath is ${dataset.rdd.getNumPartitions}")
      dataset
    } catch {
      case e: Exception => throw SparkReadException(s"Error while reading CSV $inputPath: " + e.getMessage, e)
    }

  }

  def writeCSV(outputPath: String, dataset: Dataset[Row]): String = {
    logger.info(s"Writing CSV files to $outputPath")
    try {
      dataset
        .write
        .option("header", "true")
        .option("delimiter", "|")
        .mode(SaveMode.Overwrite)
        .csv(outputPath)
      outputPath
    } catch {
      case e: Exception => throw SparkWriteException(s"Error while writing CSV $outputPath => ${e.getMessage}", e)
    }
  }
}

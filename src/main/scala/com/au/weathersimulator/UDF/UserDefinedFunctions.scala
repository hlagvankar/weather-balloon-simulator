package com.au.weathersimulator.UDF

import com.au.weathersimulator.service.Location
import com.au.weathersimulator.simulator.{DefaultDistanceUnits, DefaultTempUnits, Observatories}
import org.apache.spark.sql.functions._

/*
 * UDF to normalize data.
 */
object UserDefinedFunctions {

  var lastLocation: (Float, Float) = (0F, 0F)
  var totalDistance: Double = 0F

  val udf_temp_normalize = udf((temperature: Int, observatory: String) => {
    if (observatory == Observatories.AUSTRALIA)
      temperature
    else if (observatory == Observatories.UNITED_STATES)
      celsiusToFahrenheit(temperature)
    else
      celsiusToKelvin(temperature)
  })

  val udf_convert_distance = udf((location: String, observatory: String) => {
    val latitude = location.split(",")(0)
    val longitude = location.split(",")(1)
    val locations = Location(latitude.toDouble, longitude.toDouble)
    observatory match {
      case Observatories.UNITED_STATES => kilometerToMiles(locations.latitude).toString + "," + kilometerToMiles(locations.longitude).toString
      case Observatories.FRANCE => kilometerToMeter(locations.latitude).toString + "," + kilometerToMeter(locations.longitude).toString
      case _ => locations.latitude.toString + "," + locations.longitude.toString
    }

  })

  val udf_get_temperature_unit = udf((observatory: String) =>
    if(observatory == Observatories.AUSTRALIA)
      DefaultTempUnits.CELSIUS
    else if(observatory == Observatories.UNITED_STATES)
      DefaultTempUnits.FAHRENHEIT
    else
      DefaultTempUnits.KELVIN
  )

  val udf_get_location_unit = udf((observatory: String) =>
    if(observatory == Observatories.UNITED_STATES)
      DefaultDistanceUnits.MILES
    else if(observatory == Observatories.FRANCE)
      DefaultDistanceUnits.METER
    else
      DefaultDistanceUnits.KILOMETERS
  )

  def celsiusToKelvin(celsius: Int) = celsius + 273.15

  def fahrenheitToKelvin(fahrenheit: Int) = (fahrenheit + 459.67) * (5 / 9)

  def fahrenheitToCelsius(fahrenheit: Int) = (fahrenheit - 32) * (5 / 9)

  def celsiusToFahrenheit(d: Double) = d * 9 / 5 + 32

  def calculateDistance(location:(Float, Float), observatory: String) = {
    val distanceTravelled = math.sqrt(math.pow(location._1 - lastLocation._1, 2) + math.pow(location._2 - lastLocation._2,2))
    lastLocation = location
    totalDistance = totalDistance + distanceTravelled
    if(observatory == Observatories.AUSTRALIA)
      (totalDistance.round, observatory, DefaultDistanceUnits.KILOMETERS)
    else if(observatory == Observatories.UNITED_STATES)
      (kilometerToMiles(totalDistance).round, observatory, DefaultDistanceUnits.MILES)
    else if(observatory == Observatories.FRANCE)
      (kilometerToMeter(totalDistance).round, observatory, DefaultDistanceUnits.METER)
    else
      (totalDistance.round, observatory, DefaultDistanceUnits.KILOMETERS)
  }

  private def kilometerToMiles(distance: Double): Double = {
    distance / 1.609344F
  }

  private def kilometerToMeter(distance: Double): Double = {
    distance * 1000
  }

}
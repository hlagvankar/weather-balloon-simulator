package com.au.weathersimulator.run

import com.au.weathersimulator.io.SparkIO
import com.au.weathersimulator.service.RunService._
import com.au.weathersimulator.utils.{AppConfig, RunnerUtil}
import com.typesafe.config.ConfigFactory
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

/*
 * This is the entry point of the program.
 * Program gets all inputs passed via CLI, creates SparkContext and SparkSession and then run the service.
 */
object Run {

  private val logger = LoggerFactory.getLogger(Run.getClass)

  def main(args: Array[String]): Unit = {
    // get input arguments
    val options = RunnerUtil.nextOption(Map(), args.toList)

    val appName = options.get('appName)
    val inputPath = options('inBucket)
    val outputPath = options('outBucket)
    val numberOfRecords = options('numberOfRecords)
    val mode = options.get('mode)
    val config = ConfigFactory.load().toString

    if (appName.isEmpty) {
      throw new IllegalArgumentException("Class name cannot be empty")
    }

    if (mode.isEmpty) {
      throw new IllegalArgumentException(s"Mode can't be empty. Specify --local-flag with value as Y/N")
    }
    logger.info(s"Running app ${appName.get}")

    val sparkIO = new SparkIO
    val appConfig = new AppConfig(config)

    val conf = if (mode == "N") new SparkConf().setAppName(appName.get) else new SparkConf().setAppName(appName.get).setMaster("local[*]")
    val sc = sparkIO.createSparkContext(appConfig, conf, appName.get)
    logger.info(s"Spark Context created successfully")

    implicit val sparkSession: SparkSession = sparkIO.createSparkSession(appConfig, conf, appName.get)
    logger.info(s"Spark Session created successfully")

    try {
      runSparkJobs(appConfig, sparkIO, appName, inputPath, outputPath, numberOfRecords.toInt, sc)
    } finally {
      sparkSession.stop()
    }
  }

}

package com.au.weathersimulator.builder

trait Builder[T] {

  def execute: T
}

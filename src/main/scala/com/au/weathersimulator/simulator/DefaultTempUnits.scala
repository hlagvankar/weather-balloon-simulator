package com.au.weathersimulator.simulator

object DefaultTempUnits {
  val KELVIN = "kv"
  val CELSIUS = "celsius"
  val FAHRENHEIT = "fahrenheit"
}
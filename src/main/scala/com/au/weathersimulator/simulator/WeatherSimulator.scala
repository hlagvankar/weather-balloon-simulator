package com.au.weathersimulator.simulator

import com.au.weathersimulator.utils.common._
import org.apache.spark.SparkContext
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.slf4j.LoggerFactory

object WeatherSimulator {

  private val logger = LoggerFactory.getLogger(WeatherSimulator.getClass)

  def execute(inputPath: String, numberOfRecords: Int, spark: SparkSession, sc: SparkContext): Unit = {
    logger.info("Starting weather data generator...")
    logger.info(s"Writing weather data to $inputPath")

    writeWeatherData(numberOfRecords, sc, spark)
      .write
      .mode(SaveMode.Overwrite)
      .option("header", "true")
      .option("delimiter", "|")
      .csv(inputPath)
  }
}














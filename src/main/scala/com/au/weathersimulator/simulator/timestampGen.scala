package com.au.weathersimulator.simulator

import java.time.LocalDateTime

import com.au.weathersimulator.generator.Generator

import scala.util.Random

object timestampGen extends Generator[LocalDateTime] {
  // generate timestamp of current year
  override def generate: LocalDateTime = LocalDateTime.of(2018, 2, Random.nextInt(2) + 1, Random.nextInt(24), Random.nextInt(60))
}

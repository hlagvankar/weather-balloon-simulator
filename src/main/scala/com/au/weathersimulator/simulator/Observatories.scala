package com.au.weathersimulator.simulator

object Observatories {
  val AUSTRALIA = "AU"
  val UNITED_STATES = "US"
  val FRANCE = "FR"
  val ITALY = "IT"
  val INDIA = "IN"
  val SINGAPORE = "SG"
  val CANADA = "CA"
}

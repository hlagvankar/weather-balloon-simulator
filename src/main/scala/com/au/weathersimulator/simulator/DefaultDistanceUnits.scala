package com.au.weathersimulator.simulator

object DefaultDistanceUnits {
  val KILOMETERS = "km"
  val MILES = "miles"
  val METER = "m"
}

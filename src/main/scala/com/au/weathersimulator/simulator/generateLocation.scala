package com.au.weathersimulator.simulator

import com.au.weathersimulator.generator.Generator

import scala.util.Random

object generateLocation extends Generator[(Double, Double)] {
  def generate: (Double, Double) = (Random.nextDouble + Random.nextInt(5), Random.nextDouble + Random.nextInt(1000))
}
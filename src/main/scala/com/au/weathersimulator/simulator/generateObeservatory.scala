package com.au.weathersimulator.simulator

import com.au.weathersimulator.generator.Generator

import scala.util.Random

object generateObeservatory extends Generator[String] {

  import Observatories._
  import DefaultDistanceUnits._
  import DefaultTempUnits._

  val observatoryCodes: Map[String, (String, String)] =
    Map.empty[String, (String, String)]
      .withDefault(_ => (KELVIN, KILOMETERS))
      .updated(AUSTRALIA, (CELSIUS, KILOMETERS))
      .updated(UNITED_STATES, (FAHRENHEIT, MILES))
      .updated(FRANCE, (KELVIN, METER))
      .updated(ITALY, (KELVIN, KILOMETERS))
      .updated(INDIA, (KELVIN, KILOMETERS))
      .updated(SINGAPORE, (KELVIN, KILOMETERS))
      .updated(CANADA, (KELVIN, KILOMETERS))

  def generate: String = {
    observatoryCodes.keys.drop(Random.nextInt(observatoryCodes.keys.size)).head
  }
}

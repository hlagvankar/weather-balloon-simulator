package com.au.weathersimulator.simulator

import com.au.weathersimulator.generator.Generator

import scala.util.Random


object temperatureGen extends Generator[Int] {
  // Assume min and max temperature ever achieved on earth
  val minimumTempCelsius = -88
  val maximumTempCelsius = 58

  def generate: Int = {
    val temperatureRange = maximumTempCelsius - minimumTempCelsius
    //if (Random.nextBoolean) -Random.nextInt(100) else Random.nextInt(100)
    Random.nextInt(temperatureRange) + minimumTempCelsius
  }

}
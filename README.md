# weather-balloon-simulator

This simluates weather data using Spark/Scala. Program generates given number of input data, then reads it and then calculates various statistics.  

##### Pre-requisites
1. Hadoop installation  
2. Spark installation  
3. If you're running using IntelliJ you need to provide Spark jars directory to run this program using IntelliJ
  
#### How to Run
1. Download this repository  
2. Run `sbt clean package`
3. submit the spark job as below  
   `spark-submit --class com.au.weathersimulator.service.Run --master yarn --deploy-mode cluster <application-jar-file> <program-arguments>`  
4. Currently program accepts below arguments  
    `--in-bucket: Input path where data to be generated. This is the same path from which spark will read input files e.g. hdfs:/// OR file:///`  
    
    `--out-bucket: Output path where spark stores its output. e.g. hdfs:/// OR file:///`  
      
    `--number-of-records: Input records to be generated. In our case we need to generate 500 million of records`  
    
    `--conf: Additional configuration. Can be used for testing`  
    
    `--app-name: Application Class Name`  
    
    `--local-mode: Local mode flag. Values are Y/N. If you're running program locally then supply value as Y`  

#### Assumptions
1. Initially all temperature and location units are considered to be Celsius and KM respectively.
2. If any value is null filter out those records before processing. 

#### Statistics to be calculated
1. ##### The minimum temperature:  
    Minimum temperature is calculated daily per observatory. Initial units are considered to be in Celsius.
2. ##### The maximum temperature:  
    Maximum temperature is calculated daily per observatory. Initial units are considered to be in Celsius.
3. ##### The mean temperature:  
    Mean temperature is assumed to be daily's ((Max Temp + Min Temp) / 2).
4. ##### The number of observations from each observatory:  
    This is calculated as SELECT count(*) FROM <input> GROUP BY observatory.
5. ##### Total distance travelled:  
    Location units are considered to be in KM initially.
6. ##### Normalize input data and store it:
    Input is normalize based on above assumptions.
    
#### Tests  
   I have added few tests around UDF & sparkIO. On similar line we can tests other classes as well.  
   You can run tests with `sbt test`      
    
#### To be implemented
   Spark program should calculate statistics on request. Currently spark calculates all the statistics.    